#include <iostream>
#include <string> 
//Class of magic spells for the game
//All spells have damage values and energy cost

class Magic_attacks
{
private:
    int min_damage, max_damage;//these values are not accessed out of the class so they can be private
    
protected:
    int energy_cost, damage;//can be accessed by child classes only

    Magic_attacks(int _min_damage, int _max_damage, int _energy_cost)
    {
        min_damage = _min_damage;
        max_damage = _max_damage;
        damage = rand() % (max_damage - min_damage + 1) + min_damage;
        
        // ^ damage sets the damage of the spell to a random value between max and min damage,
        //so that each spell is slightly different strengths
        
        energy_cost = _energy_cost;
    };
    
public://two get functions so the damage and energy cost can be accessed out of the class
    int get_damage()
    {
        return damage;
    }
    
    int get_energy_cost()
    {
        return energy_cost;
    }
};

class Water_bucket: public Magic_attacks//three child classes defining the stats of each spell
{
public:
    Water_bucket() : Magic_attacks(30, 50, 1000) {}
};

class Fire_blast: public Magic_attacks 
{
public:
    Fire_blast() : Magic_attacks(100, 200, 1000) {}
};

class Energy_ball: public Magic_attacks
{
public:
    Energy_ball() : Magic_attacks(250, 550, 1000) {}
};