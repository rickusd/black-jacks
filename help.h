#include <iostream>
#include <fstream>
#include <string>

void get_help_list()
{
    std::string helpCommands;
    std::ifstream fd ("keywords.txt");
    std::cout << "----------------------------------HELP MENU-----------------------------" << std::endl;
    std::cout << "IN ORDER TO DO ANY ACTION YOUR COMMAND SHOULD INCLUDE ANY OF THESE LINES" << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    while(!fd.eof())
    {
        std::getline(fd, helpCommands);
        std::cout << helpCommands << std::endl;
        helpCommands = "";
    }
    std::cout << "------------------------------------------------------------------------" << std::endl;
}