#include <iostream>
#include <string> 
//Class of enemies for the game
//All enemies have HP, damage values, crit chance and what species they are

class Enemy //parent class initializing the stats for enemies
{
public:
    int HP, min_damage, max_damage, damage;
    float crit_chance, dodge_chance;
    std::string species;

    Enemy(int _HP, int _min_damage, int _max_damage, float _crit_chance, float _dodge_chance, std::string _species)
    {
        HP = _HP;
        min_damage = _min_damage;//min and max used to calculate damage output
        max_damage = _max_damage;
        crit_chance = _crit_chance;//chance of enemy hitting a critical hit
        dodge_chance = _dodge_chance;//chance of enemy dodging attack
        damage = rand() % (max_damage - min_damage + 1) + min_damage;
        
        // ^ damage sets the damage of the enemy to a random value between max and min damage,
        //so that each enemy is slightly different strengths
        
        species = _species;//species of monster
    }

    void set_hp(int health)//setter for hp to allow health to be changed
    {
        HP = health;
    }
    
    int get_hp()//getter for hp allows the attributes value to be seen
    {
        return HP;
    }
};

class Puppy: public Enemy //first enemy, weak with 0 chance of hitting a crit
{
public:
    Puppy() : Enemy( 100, 10, 20, 0, 0, "Puppy" ) {}
};

class Skeleton: public Enemy //second enemy becomes stronger, including a crit chance
{
public:
    Skeleton() : Enemy( 200, 25, 50, 0.1, 0.1, "Skeleton" ) {}
};

class Ogre: public Enemy
{
public:
    Ogre() : Enemy( 500, 45, 70, 0.25, 0.15, "Ogre" ) {}
};

class Monster_king: public Enemy //final boss with the highest stats
{
public:
    Monster_king() : Enemy( 1500, 100, 200, 0.33, 0.20, "Monster King" ) {}
};