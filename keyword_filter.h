#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::vector< std::vector <std::string> >read_keywords()
{
    std::string wordsInList;
    std::vector<std::string> tempList;
    std::vector<std::vector<std::string> >finalList;
    std::vector<std::string> readingVector;
    std::ifstream fd("keywords.txt");
    while (!fd.eof())
    {
        std::getline(fd, wordsInList);
        readingVector.push_back(wordsInList);
    }
    wordsInList = "";
    for (int i = 0; i < readingVector.size(); i++)
    {
        for (int j = 0; j < readingVector[i].size(); j++)
        {
            if (readingVector[i].at(j) == ',' || j == readingVector[i].size() - 1)
            {
                tempList.push_back(wordsInList);
                wordsInList = "";
            }
            else
            {
                wordsInList += readingVector[i].at(j);
            }
        }
        finalList.push_back(tempList);
        tempList.clear();
    }
    return finalList;
}
int keyword_filter(std::string commandLine)
{
    std::vector<std::vector < std::string > >keywords = read_keywords();
    for (int i = 0; i < keywords.size(); i++)
    {
        for (int j = 0; j < keywords[i].size(); j++)
        {
            size_t found = commandLine.find(keywords[i][j]);
            if (found != std::string::npos)
            {
                return i;
            }
            
        }
    }
    
}