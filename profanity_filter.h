#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::vector<std::string> read_list()
{
    std::string word;
    std::vector<std::string> wordList;
    std::ifstream fd("swearWords.txt");
    while(!fd.eof())
    {
        fd >> word;
        wordList.push_back(word);
    }
    return wordList;
}
std::vector<std::string> sentence_to_vector(std::string sentence)
{
    std::istringstream ss(sentence);
    std::string word;
    std::vector<std::string> wordsInSentence;
    while(ss >> word)
    {
        wordsInSentence.push_back(word);
    }
    return wordsInSentence;
}
std::string profanity_filter(std::string sentence)
{
    std::vector<std::string> wordsInSentence = sentence_to_vector(sentence);
    std::vector<std::string> profanityFilter = read_list();
    std::string finalSentence;
    std::string tempWord;
    for (int i = 0; i < wordsInSentence.size(); i++)
    {
        for (int j = 0; j < profanityFilter.size(); j++)
        {
            if (wordsInSentence[i] == profanityFilter[j])
            {
                for (int k = 0; k < wordsInSentence[i].size(); k++)
                {
                    tempWord += "*";
                }
                wordsInSentence[i] = tempWord;
            }
        }
    }
    for (int i = 0; i < wordsInSentence.size(); i++)
    {
        finalSentence += wordsInSentence[i] + " ";
    }
    return finalSentence;

}