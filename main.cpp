#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>
#include <cctype>
#include "database.h"
#include "loot_chest.h"
#include "main_charecter.h"
#include "profanity_filter.h"
#include "store_class.h"
#include "keyword_filter.h"
#include "help.h"

int main()
{
    std::string userName;
    std::string id;
    std::string command;
    read_item_database();
    read_store_database();
    std::cout << "Enter your name: ";
    std::cin >> userName;
    profanity_filter(userName);
    Player mc;
    Shop MainStore;
    loot_chest chest;
    mc.welcome_message(userName);
    
    /// User input throughout whole game
    bool commandCondition = true;
    command = "";
    while (commandCondition)
    {
        std::getline(std::cin,command);
        switch (keyword_filter(command))
        {
            case 0:
                MainStore.open_store();
                break;
            case 1:
                MainStore.open_store();
                std::cout << "Enter item's ID: ";
                std:: cin >> id;
                MainStore.buy_item(id);
                break;
            case 2:
                mc.print_stats();
                break;
            case 3:
                mc.open_inventory();
                std::cout << "Enter item's ID: ";
                std:: cin >> id;
                mc.activate_item(id);
                break;
            case 4:
                mc.open_inventory();
                break;
            case 5:
                chest.open_chest();
                break;
            case 6:
                get_help_list();
                break;
            case 7:
                commandCondition = false;

        }
    }
    
} 