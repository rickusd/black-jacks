#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
class Shop: public Player
{
    public:
    Shop() : Player() {}
    void open_store()
    {
        std::cout << "-----------------------------------------------------------------------------------------------------------------" << std::endl;
        std::cout << std::fixed << std::left << std::setw(50) << "" << "DEEP FOREST SHOP" << std::endl;
        std::cout << "-----------------------------------------------------------------------------------------------------------------" << std::endl;
        std::cout << std::fixed << std::left << std::setw(10) << "ID" << " " << std::fixed << std::left << std::setw(22) << "ITEM NAME" << " " << std::fixed << std::left << std::setw(8) << "TYPE"  << std::fixed << std::left << std::setw(10) << "PRICE" << " " << std::fixed << std::left << std::setw(60) << "DESCRIPTION" << std::endl;
        std::cout << "-----------------------------------------------------------------------------------------------------------------" << std::endl;
        for (int i = 0; i < Store.size(); i++)
        {
            std::cout << std::fixed << std::left << std::setw(10) << Store[i].id << " " << std::fixed << std::left << std::setw(22) << Store[i].name << " " << std::fixed << std::left << std::setw(8) << Store[i].type  << std::fixed << std::left << std::setw(10) << Store[i].price << " " << std::fixed << std::left << std::setw(60) << Store[i].description << std::endl;
        }
        std::cout << "-----------------------------------------------------------------------------------------------------------------" << std::endl;
    }
    void buy_item(std::string id)
    {
        for (int i = 0; i < Store.size(); i++)
        {
            if (Store[i].id == id)
            {
                if(Store[i].price <= Player::gold)
                {
                    Player::add_to_inventory(Store[i].id);
                    Player::gold = Player::gold - Store[i].price;
                    std::cout << "Succesfully bought" << std::endl;
                }
                else
                {
                    std::cout << "You don't have enough gold..." << std::endl;
                }
                
            }
        }
    }

    
    
};