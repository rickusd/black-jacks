#include <iostream>
#include <string>
#include <vector>
#include "libsqlite.hpp"

struct items
{
    std::string id;
    std::string name;
    std::string type;
    int value;
    int chance;
}; 
std::vector<items>Items;

struct shop_items
{
    std::string id;
    std::string name;
    std::string type;
    int value;
    int price;
    std::string description;
};
std::vector<shop_items>Store;

void read_item_database()
{
    int i = 0;
    sqlite::sqlite db("database.sqlite");
    auto cur = db.get_statement();
    cur -> set_sql("SELECT * FROM Items;");
    cur -> prepare();
    
    while(cur -> step())
    {
        Items.push_back(items());
        Items[i].id = (cur->get_text(0));
        Items[i].name = (cur->get_text(1));
        Items[i].type = (cur->get_text(2));
        Items[i].value = (cur->get_int(3));
        Items[i].chance = (cur->get_int(4));
        i++;
    }
}
void read_store_database()
{
    int j = 0;
    sqlite::sqlite db("game_shop");
    auto cur1 = db.get_statement();
    cur1 -> set_sql("SELECT * FROM GAME_SHOP;");
    cur1 -> prepare();
    
    while(cur1 -> step())
    {
        Store.push_back(shop_items());
        Store[j].id = (cur1->get_text(0));
        Store[j].name = (cur1->get_text(1));
        Store[j].type = (cur1->get_text(2));
        Store[j].value = (cur1->get_int(3));
        Store[j].price = (cur1->get_int(4));
        Store[j].description = (cur1->get_text(5));
        j++;
    }
}
        
        