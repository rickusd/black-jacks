create database SHOP;
use SHOP;
create table GAME_SHOP(ID varchar(10), NAME varchar(20),TYPE VARCHAR(10) NOT NULL, INCREASE_PERCENTAGE mediumint not null, PRICE mediumint not null, DESCRIPTION VARCHAR(60), primary key (ID));
insert into GAME_SHOP(ID, NAME, TYPE, INCREASE_PERCENTAGE, PRICE, DESCRIPTION)
VALUES("MFP_1","MOVE FASTER POTION 1","MP", 20, 500,"MAKES YOU MOVE FASTER 20 PERCENT"),("MFP_2","MOVE FASTER POTION 2","MP", 50, 800,"MAKES YOU MOVE FASTER 50 PERCENT"),("HQ1","HEAL QUICKER 1","HP", 10, 800,"MAKES YOU HEAL QUICKER 10 PERCENT"),("ARM_1","ARMOUR POTION 1","HP", 20, 500,"DEALS 20 PERCENT LESS DAMAGE FROM ENEMY"),("ARM_2","ARMOUR POTION 2","HP", 50, 800,"DEALS 50 PERCENT LESS DAMAGE FROM ENEMY"),("HEA_1","HEALTH POTION 1","HP", 25, 1500,"INCREASES HEALTH 25 PERCENT MORE"),("HEA_2","HEALTH POTION 2","HP", 50, 2000,"INCREASES HEALTH 50 PERCENT MORE"),("WEP_1", "GOLDEN AXE","AD", 25,1500,"DEALS 25 PERCENT MORE DAMAGE TO THE ENEMY"),("WEP_2", "VALYRIAN STELL SWORD","AD", 30,2500,"DEALS 30 PERCENT MORE DAMAGE TO THE ENEMY"),("WEP_3", "DOUBLE BALDE","AD", 25,1500,"DEALS 25 PERCENT MORE DAMAGE TO THE ENEMY"),("WEP_4", "CROSSBOW","AD", 35,2000,"DEALS 35 PERCENT MORE DAMAGE TO THE ENEMY WITH SILVER BOLTS"),("WEP_5", "SILVER BOLTS","AD", 35,200,"DEALS 35 PERCENT MORE DAMAGE TO THE ENEMY WITH CROSSBOW");
SELECT * FROM GAME_SHOP

