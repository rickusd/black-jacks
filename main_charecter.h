#include <iostream>
#include <string>
#include <iomanip>
#include <vector>

class Player: public loot_chest
{
    public:
    static int hp, mp, armour, ad;
    static int gold;
    std::string name;
    static std::vector<std::string> inventory;
    Player()
    {
        read_item_database();

    }
    void welcome_message(std::string name)
    {
        std::cout << "Hello, " << name << std::endl; 
    }
    void print_stats()
    {
        std::cout <<"---------Charecter stats---------" << std::endl;
        std::cout << std::fixed << std::left << std::setw(20) << "Health points: " << hp << std::endl;
        std::cout << std::fixed << std::left << std::setw(20) << "Mana points: " << mp << std::endl;
        std::cout << std::fixed << std::left << std::setw(21) << "Armour: " << armour << std::endl;
        std::cout << std::fixed << std::left << std::setw(21) << "Attack damage: " << ad << std::endl;
    }
    void activate_item(std::string item_id)
    {
        for (int i = 0; i < Items.size(); i++)
        {
            if (Items[i].id == item_id)
            {
                if (Items[i].type == "HP")
                {
                    hp += Items[i].value;
                        if(hp > 100)
                            hp = 100;
                }
                        
                if (Items[i].type == "MP")
                {
                    mp += Items[i].value;
                        if(mp > 100)
                            mp = 100;
                }
 
                if (Items[i].type == "AD")
                {
                    //Will add when enemy class will be coded
                }
                        
            }
            // Add code to procces items bought from shop
        }           
    }
    
    void open_inventory()
    {
        std::cout <<"---------Charecter inventory---------" << std::endl;
        for (int i = 0; i < inventory.size(); i++)
        {
            for(int j = 0; j < Items.size(); j++)
            {
                if(inventory.at(i) == Items[j].id)
                    std::cout << i+1 << ". " << Items[j].name << " which is " << Items[j].type << " consumable with value " << Items[j].value << std::endl;
            }
            for(int k = 0; k < Store.size(); k++)
            {
                if(inventory.at(i) == Store[k].id)
                    std::cout << i+1 << ". " << Store[k].name << " which " << Store[k].description << std::endl;
            }
            
        }
        std::cout <<"-------------------------------------" << std::endl;
            
    }
    void add_to_inventory(std::string item_id)
    {
        inventory.push_back(item_id);
    }
    void pick_up_object()
    {

        int random_value = loot_chest::random_number(5);
        switch (loot_chest::random_number(4))
        {
            case 1:
                std::cout << "Congratulations, you recieved +" << random_value << " HEALTH POINTS" << std::endl;
                hp += random_value;
                break;
            case 2:
                std::cout << "Congratulations, you recieved +" << random_value << " MANA POINTS" << std::endl;
                mp += random_value;
                break;
            case 3:
                std::cout << "Congratulations, you recieved +" << random_value << " ARMOUR POINTS" << std::endl;
                armour += random_value;
                break;
            case 4:
                std::cout << "Congratulations, you recieved +" << random_value << " ATTACK DAMAGE POINTS" << std::endl;
                ad += random_value;
                break;
        }
    }
    void attack_enemy()
    {
        //Will add when enemy class will be coded
    }
    
    
    
};
int Player::gold = 1000;
std::vector<std::string> Player::inventory;
int Player::hp = 100;
int Player::mp = 100;
int Player::armour = 20;
int Player::ad = 20;